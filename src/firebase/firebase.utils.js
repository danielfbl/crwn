import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
  apiKey: 'AIzaSyDU_fbtXLAq1b1Y7InrDD-ikOuiR1K55ME',
  authDomain: 'crwn-db-dc54c.firebaseapp.com',
  databaseURL: 'https://crwn-db-dc54c.firebaseio.com',
  projectId: 'crwn-db-dc54c',
  storageBucket: 'crwn-db-dc54c.appspot.com',
  messagingSenderId: '446614607924',
  appId: '1:446614607924:web:1fed797d6e7d3a8159d261',
  measurementId: 'G-MB16BNWPMH'
};

export const createUserProfileDocument = async (userAuth, additionalData) => {
  if (!userAuth) return;

  const userRef = firestore.doc(`users/${userAuth.uid}`);
  const snapShot = await userRef.get();

  if (!snapShot.exists) {
    const { displayName, email } = userAuth;
    const createdAt = new Date();

    try {
      await userRef.set({
        displayName,
        email,
        createdAt,
        ...additionalData
      });
    } catch (error) {
      console.log('Error creating user', error.message);
    }
  }

  return userRef;
};

firebase.initializeApp(config);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: 'select_account' });
export const SignInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;
